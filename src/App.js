import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Welcome to new FD website!</h1>
      </div>
    );
  }
}

export default App;
